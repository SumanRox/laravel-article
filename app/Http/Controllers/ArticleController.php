<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Article;
use App\Http\Resources\Article as ArticleResource;

class ArticleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //  Get the list of article
        $article=Article::orderBy('created_at','desc')->paginate(5);
        // Return collection of article as a resource
        return ArticleResource::collection($article);
    }

    /**
     * Show the form for creating a new resource.
     * 
     * @return \Illuminate\Http\Response
     */


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Storing the data in a database
        // First check if the data already exist and needs to be updated
        // and then proceed based on the result
        $article=$request->isMethod('put')? Article::findOrFail($request->article_id) : new Article;

        // Save the data
        $article->id=$request->input('article_id');
        $article->title=$request->input('title');
        $article->body=$request->input('body');

        // Now if the saving is success, lets return the resource
        if($article->save()){
            return new ArticleResource($article);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // Get the article
        $article=Article::findOrFail($id);
        // Once we got it, render it as a resource
        return new ArticleResource($article);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
                // Get the article
                $article=Article::findOrFail($id);
                // Once we got it, delete it
                if($article->delete()){
                    
                    return new ArticleResource($article);
                }
    }
}
